# README #

Just do a docker-compose up -d and you should be ready to go using your own local GoCD server with Java debugging enabled. 5005 on the server-container, 5006 on the agent-container.

getting a new plugin onto the server is as easy as copy your jar file to .../data/lb/plugins/external and wait a bit :-)

Hint: If something goes wrong with "access permissions" under .../data, just do a chmod -R a+rw data. This is a *dev - env*, dude (and nothing like production/live/...).